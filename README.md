# sources-graph
A Graph Database of the sources for any piece of news and a label to assert whether these sources are unknown, trustworth, fake, untrue or unreliable.

# Categories of sources and information
Sources can be in order
of how valid they are:

##### Trustworthy = 1
A source is trustworthy if several
independant checks have verified it.

##### Unknown (default) = 0
A source is unknown if it's
not known whether the source 
is trustworth, fake, untrue or unreliable.

##### Unreliable = -1
A source that is authored,
published, edited by an entity or person
known for publishing untrue or fake news.

This is also a label given to any
circular sources.
(A is a source of B, B a source of A).

##### Untrue  = -2
A source is untrue if several
independant checks have falsified.

##### Fake = -3
A source that isn't just untrue, but
also intentionally misleading to benefit
some (political, economic, other) interested
parties.

# Function of the graph
A node object contains these fields:
```protobuf
message Node {
    cid : CID
    label : Label
    isCheck : Bool
}
```
There are two kinds of edges:
1. References (from reference to refering source)
2. Checks (from check to source checked)

Checks also contain a recommendation for 
a label.

# Usage
The Database is an OrbitDB Database.

There are no restrictions on adding sources and
their references,
if they are not checks. Though the label
will remain `unknown` by default.

In order to ensure that nobody can
spam the system with spams, aynone adding
checks has to create an account.

These accounts then publish checks.

Adding a check requires having an account.
This account contains these fields:
```protobuf
message Account {
    sources : [CID]
    validity : Float
    profile : CID
}
```
- `sources` are the CID of the sources/checks
that this account has published.

- `validity` is a float that indicates how many
checks from this account are vaild.
- `profile` is the CID of the profile of this account.

## Labeling
1. A Source is added
2. References are added
3. Check added by accounts
4. Calculate accounts validity
5. Labeling

Repeat 3 to 5 if more references are added.

In order to give a label other than `unknown`
to a source, certain conditions have to met:

1. A minimum of N independent checks must have been made. (By different accounts)
2. The label that has the most votes * account validation.
3. If the references are circular, the highest possible label is `unreliable`. All other checks are dropped.

## Validity calculation
To calculate the validity of an account
each of their checks and recommendations are taken
compared with the current label of the checked 
sources and is subtracted from the recommendation.

Then the absolute of the sum of those differences
is the validity.

