class Index {
    static INDEX = {
        nodes: [],
        edges: [],
        accounts: [],
    }
    
	constructor() {
		this._index = Object.copy({}, INDEX)
	}

    /**
     * @param source {string} key of the source in nodes
     * @returns {Array<Source>} reference of source
     */
    references(source) {
        return this._index.edges.filter(e =>
                                        e.referencee == source
                                       )
    }
    
	/**
	 * Operations:
	 * ADDSOURCE
	 * ADDREFERENCE
	 *
	 * CHANGEPROFILE
	 */
	updateIndex(oplog) {
        this._index = Object.copy(
		oplog.values.reduce((handled, item) => {
			if(!handled.includes(item.hash)) {
				handled.push(item.hash)
                
                const payload = item.payload
				switch (payload.op) {
				case "ADDSOURCE":
                    if(!payload.isCheck || item.identity.id == payload.account) {
                        const source = new Source(payload)
                        this._index.nodes[item.hash] = node
                    }

                    if(accounts[item.identity.id] === undefined) {
                        accounts[item.identity.id] = new Account([], 0, "", item.identity.id)
                    }

                    accounts[item.identity.id].sources.push(item.hash)
                    break

                case "ADDREFERENCE":
                    const referenced = this._index.nodes[item.payload.referenced]
                    const referencee = this._index.nodes[item.payload.referencee]

                    if(item.identity.id == referencee.account) {
                        this.index.edges[item.hash] = new Reference(referenced, referencee)
                    }
                    break
                    
                case "CHANGEPROFILE":
                    const account = this._index.accounts[item.identity.id]
                    account.profile = payload.cid
				}
			}
		}, [])
	}
}
