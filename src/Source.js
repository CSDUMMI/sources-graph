class Source {
	constructor(payload) {
		this.cid = payload.cid
		this.label = payload.label
		this.isCheck = payload.isCheck
        this.account = payload["account"] != undefined ? payload.account : null
    }

	dump() {
		return JSON.stringify({
			"cid": this.cid,
			"label": this.label,
			"isCheck": this.isCheck
		})
	}
}

class Reference {
    constructor(referenced, referencee) {
        this.referenced = referenced
        this.referencee = referencee
    }
}
